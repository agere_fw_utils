/*
 * This is a wrapper for hcfcfg.h included in wl_lkm that redefines
 * hcf_32 to be 32-bit on any architecture.
 */
#include <stdint.h>
#include "include/hcf/hcfcfg.h"
#define hcf_32 uint32_t
