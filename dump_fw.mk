#
# Makefile for wlags49_cs_xxx
#

CC = gcc

# NOTE if compiling wl_lkm_714:
#  - the firmware files need to be modified to include "mmd.h" instead of "..\hcf\mmd.h"
#  - include/hcf/hcfcfg.h line 775 should be commented out
#  - set LKM on next line to 714 718 or 722
LKM=718

ifeq ($(LKM),714)
 DIR_FW := dhf
 LKM_CFLAGS = -D__wl_lkm=714
endif
ifeq ($(LKM),718)
 DIR_FW := firmware
 LKM_CFLAGS := -DHCF_DLV -D__wl_lkm=718
endif
ifeq ($(LKM),722)
 DIR_FW :=hcf
 LKM_CFLAGS := -D__wl_lkm=722
endif

DIR_HCF         := hcf
DIR_DHF         := dhf
DIR_CONFIG      := include/hcf
DIR_WIRELESS    := include/wireless

OBJS :=      dump_fw.o

CFLAGS := -O3 -Wall -Wstrict-prototypes -pipe
CPPFLAGS := -I. -I$(DIR_CONFIG) -I$(DIR_HCF) -I$(DIR_DHF)

H25_OBJS  := $(DIR_FW)/ap_h25.o $(DIR_FW)/sta_h25.o

H2_OBJS   := $(DIR_FW)/ap_h2.o $(DIR_FW)/sta_h2.o
H2_CFLAGS := -DHCF_TYPE=4

H1_OBJS   := $(DIR_FW)/ap_h1.o $(DIR_FW)/sta_h1.o
H1_CFLAGS := -DHCF_TYPE=0

TARGETS:=hfwget

IN_ROOT:=n
# Check if we are in wl_lkm root
ifneq (,$(findstring $(DIR_FW),$(wildcard $(DIR_FW))))
 ifneq (,$(findstring $(DIR_HCF),$(wildcard $(DIR_HCF))))
  IN_ROOT:=y
 endif
endif

ifeq ($(IN_ROOT),y)
 ifneq ($(LKM),722)
  TARGETS+=dump_h1_fw dump_h2_fw
 else
  TARGETS+=dump_h2_sta_fw dump_h2_ap_fw
  TARGETS+=dump_h25_sta_fw dump_h25_ap_fw
 endif
else
$(info Info: Not in wl_lkm directory, so not building wl_lkm dumpers)
endif

all: $(TARGETS)

ifneq ($(LKM),722)
dump_h1_fw : CFLAGS+= $(LKM_CFLAGS) $(H1_CFLAGS)
dump_h1_fw : $(OBJS) $(H1_OBJS)
	$(CC) $(CFLAGS) $^ -o $@

dump_h2_fw : CFLAGS+= $(LKM_CFLAGS) $(H2_CFLAGS)
dump_h2_fw : $(OBJS) $(H2_OBJS)
	$(CC) $(CFLAGS) $^ -o $@

else
$(DIR_HCF)/ap_h%.c : $(DIR_HCF)/fw_h%.c.ap
	cp $^ $@

$(DIR_HCF)/sta_h%.c : $(DIR_HCF)/fw_h%.c.sta
	cp $^ $@

dump_h2_ap_fw : CFLAGS+= $(LKM_CFLAGS)
dump_h2_ap_fw : $(OBJS) $(DIR_HCF)/ap_h2.o
	$(CC) $(CFLAGS) $^ -o $@

dump_h2_sta_fw : CFLAGS+= $(LKM_CFLAGS)
dump_h2_sta_fw : $(OBJS) $(DIR_HCF)/sta_h2.o
	$(CC) $(CFLAGS) $^ -o $@

dump_h25_ap_fw : CFLAGS+= $(LKM_CFLAGS)
dump_h25_ap_fw : $(OBJS) $(DIR_HCF)/ap_h25.o
	$(CC) $(CFLAGS) $^ -o $@

dump_h25_sta_fw : CFLAGS+= $(LKM_CFLAGS)
dump_h25_sta_fw : $(OBJS) $(DIR_HCF)/sta_h25.o
	$(CC) $(CFLAGS) $^ -o $@
endif

hfwget : hfwget.c
	$(CC) $(CFLAGS) $^ -o $@

clean :
	rm -f $(TARGETS) $(OBJS) $(H1_OBJS) $(H2_OBJS) $(H25_OBJS)
